# Setup

## Build the project

After setting up the docker on your machine and you are running the project first time please do `./make upForce`. Following this
you can use `./make up`.

## Run the tests

The tests can be invoked as simple as `./make qa`. If you decide to run tests individually (that we recommend) please ssh into the container and perform the interested one.

Bare in mind that the tests are not isolated. We have not added any `beforeScenario|Feature` hooks to ensure data isolation for the needs of this coding test. 
Each tests rely on the data that could be modified in the other test so please do not fall into the trap ;)

###### Extra
Feel free to check other `make` targets, you might find them helpful.
