<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Room;
use App\Repository\RoomRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;

final class Version20230117213212 extends AbstractMigration
{
    public function __construct(
        Connection $connection,
        LoggerInterface $logger
    ) {
        parent::__construct($connection, $logger);
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE room ADD free_space INT NOT NULL');
        $this->addSql(
            'UPDATE room 
                 SET free_space = max_capacity - (
                     SELECT COUNT(*) FROM child WHERE child.room_id = room.id AND child.archived = 0
                 )'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE room DROP free_space');
    }
}
