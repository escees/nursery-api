<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Child;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

final class ChildCollectionDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private iterable $collectionExtensions
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Child::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);

        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('e');
        $queryNameGenerator = new QueryNameGenerator();

        $meta = [];

        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);
            $paginator = new Paginator($queryBuilder->getQuery());
            if ($extension instanceof QueryResultCollectionExtensionInterface && $extension->supportsResult($resourceClass, $operationName, $context)) {
                return [
                    'meta' => [
                        'start' => $paginator->getQuery()->getFirstResult() + 1,
                        'limit' => $paginator->getQuery()->getMaxResults(),
                        'page_results' => count($paginator),
                        'total_results' => $paginator->count(),
                    ],
                    'data' => $extension->getResult($queryBuilder, $resourceClass, $operationName, $context),
                ];
            }
        }

        return [
            'meta' => $meta,
            'data' => $queryBuilder->getQuery()->getResult(),
        ];
    }
}
