<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\StatisticsController;
use App\Repository\RoomRepository;
use App\Validator\RoomNotFull;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'get_statistics' => [
            'method' => 'GET',
            'path' => '/rooms/statistics',
            'controller' => StatisticsController::class,
            'read' => false,
        ],
    ],
    itemOperations: [
        'get',
        'post',
    ],
    denormalizationContext: ['groups' => ['write:room', 'write:child']],
    normalizationContext: ['groups' => ['read:room', 'read:child']],
)]
#[Entity(repositoryClass: RoomRepository::class)]
class Room
{
    #[ApiProperty(identifier: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:room', 'read:child.room'])]
    private int $id;

    #[ORM\Column(type: 'string', length: 100)]
    #[Groups(['read:room', 'read:child.room'])]
    private string $name = '';

    #[ORM\Column(type: 'integer')]
    #[Groups(['read:room', 'read:child.room'])]
    private int $maxCapacity = 0;

    #[ORM\OneToMany(mappedBy: 'room', targetEntity: Child::class)]
    private Collection $children;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read:room', 'read:child.room'])]
    private int $freeSpace;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaxCapacity(): int
    {
        return $this->maxCapacity;
    }

    public function setMaxCapacity(int $maxCapacity): self
    {
        $this->maxCapacity = $maxCapacity;

        return $this;
    }

    /**
     * @return Collection<int, Child>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Child $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setRoom($this);
        }

        return $this;
    }

    public function removeChild(Child $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getRoom() === $this) {
                $child->setRoom(null);
            }
        }

        return $this;
    }

    public function getFreeSpace(): int
    {
        return $this->freeSpace;
    }

    public function setFreeSpace(int $freeSpace): self
    {
        $this->freeSpace = $freeSpace;

        return $this;
    }
}
