<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\ChildRepository;
use App\Validator\RoomNotFull;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    collectionOperations: [
        'get',
        'post',
    ],
    itemOperations: [
        'get',
        'post',
    ],
    attributes: ["pagination_maximum_items_per_page" => 2],
    denormalizationContext: ['groups' => ['write:child', 'write:room']],
    normalizationContext: ['groups' => ['read:child', 'read:room']]
)]
#[ApiFilter(BooleanFilter::class, properties: ['archived'])]
#[ApiFilter(GroupFilter::class, arguments: ['parameterName' => 'groups'])]
#[ApiFilter(SearchFilter::class, properties: ['room' => 'exact'])]
#[Entity(repositoryClass: ChildRepository::class)]
class Child
{
    #[ApiProperty(identifier: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('read:child')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:child', 'write:child'])]
    private string $name = '';

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:child', 'write:child'])]
    private string $surname = '';

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read:child', 'write:child'])]
    private bool $archived = false;

    #[ORM\ManyToOne(inversedBy: 'children')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:child.room','read:child', 'write:child', 'write:room'])]
    #[Assert\NotBlank]
    #[RoomNotFull(groups: ['room'])]
    #[ApiSubresource]
    private Room $room;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setRoom(Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getRoom(): Room
    {
        return $this->room;
    }
}
