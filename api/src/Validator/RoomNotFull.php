<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class RoomNotFull extends Constraint
{
    public string $message = 'There is no more space in this room.';
    public string $roomField = 'freeSpace';
}
