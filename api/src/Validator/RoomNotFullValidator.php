<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RoomNotFullValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if ($value->getFreeSpace() === 0) {
            $this->context->buildViolation($constraint->message)
                ->atPath($constraint->roomField)
                ->addViolation();
        }
    }
}
