<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class StatisticsController extends AbstractController
{
    public function __construct(
        private RoomRepository $repository,
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
    }
}
